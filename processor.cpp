#include "processor.h"
#include "math.h"

processor::processor()
{


}

void processor::setThresholdValue(int value){

    thresholdValue = value;

}



void processor::setImg(QImage &image){

    img = image;


}

/*
Here we set the base image thresholding for the Game of Life.
Image is scanned pixel by pixel and analysed.

Slightly faster (-90ms?) method would be using ScanLine() but its functionality is
uncertain if image has higher BPP rate than 32 ? This is yet to be verified.
*/

QImage processor::setImgThreshold(){

    QImage& tempimg = img;

            for (int i = 0; i < tempimg.width(); i++) {
                for (int z = 0; z < tempimg.height(); z++) {

                    QColor color = tempimg.pixelColor(i,z);
                    int calculatedColor = calcPixelColorByThreshold(color);
                    tempimg.setPixel(i, z, QColor(calculatedColor, calculatedColor, calculatedColor).rgb());


                }
            }
            return tempimg;

}

/*
Here we calculate the average of a RGB value of each pixel.
Receives a parameter QColor reference from which we take RED, GREEN, BLUE
values, sum them and divide by three (3).
Then this value is compared to a set Threshold value.
If it is higher than Threshold value, return MAX RGB value 255,
else return lowest RGB value 0.

*/

int processor::calcPixelColorByThreshold(QColor &color){

    int squareRed = color.red();
    int squareBlue = color.blue();
    int squareGreen = color.green();

    int rgbAverage = (squareRed + squareGreen + squareBlue) / 3;

    if (rgbAverage > thresholdValue){
        return 255;
    }

    else
        return 0;



}

/*
 * Here we calculate each generation for Game Of Life by analysing pixels of a picture.
 Here we use the previous image stored in this class and return the resulting QImage.
 We use GetPixel function to get pixels.

 The conditions for setting pixels alive (black) or dead (white) are as follows;
 - With less than 2 alive neighbour pixels a pixel dies or stays dead due underpopulation.
 - Pixel that is alive, having exactly 2 or 3 neighbours stays alive.
 - With exactly 3 alive neighbour pixels a pixel becomes alive.
 - With more than 3 alive neighbour pixels a pixel becomes dead due overpopulation

 Each pixel is analyzed yet the scan starts at height 1 and width one because scan
 is done in an matrix of 3*3. Pixel in the center of the matrix is the one whose neighbours are counted.
 Because of this though there occurs recounting earlier pixels again which is accounted for.

 in for loops w and h image is scanned and in further 2 foor loops i and j the matrix
 is scanned and there the neighbours are counted for and from there the rules are applied
 accordingly.
*/

QImage processor::calcGoL(){

QImage nextGenImage = img;

/*Here on we start iterating image*/

for (int w = 1; w < img.width() - 1; w++)
{

    for (int h = 1; h < img.height() - 1; h++)
    {

        int numOfNeighbours = 0;
        // these two for's are used to scan pixels neighbours
           for (int i = -1; i <= 1; i++)
               for (int j = -1; j <= 1; j++)

                 numOfNeighbours +=  getImagePixelStatus(w+i, h + j);

                        //We substract previously counted pixel out. We dont want to count it twice
                        numOfNeighbours -= getImagePixelStatus( w,h );

                        // if pixel is underpopulated, it dies
                        if (getImagePixelStatus(w,h) == 1 && numOfNeighbours < 2 ){
                            nextGenImage.setPixel(w,h, QColor(0,0,0).rgb());
                        }

                        // if pixel is overpopulated, it dies
                        else if (getImagePixelStatus(w,h) == 1 && numOfNeighbours > 3){
                            nextGenImage.setPixel(w,h, QColor(0,0,0).rgb());
                        }

                        // if a pixel is dead and has exactly 3 neighbours, it becomes alive

                        else if (getImagePixelStatus(w,h) == 0 && numOfNeighbours == 3 ){
                            nextGenImage.setPixel(w,h, QColor(255,255,255).rgb());
                        }


                        // If none of these above apply, it means pixels status is retained to next generation.
                        else
                            nextGenImage.setPixel(w,h, img.pixelColor(w,h).rgb() );



    }


}

//set the Game of Life image as the current QImage
setImg(nextGenImage);

// return the edited next gen picture
return nextGenImage;


}

/*Here we get the pixel RGB value,
 *  mainly from any channel and thus state if pixel is alive or dead.
 * This is because in a black pixel all channels have value of 255 and white ones have 0.


  */
int processor::getImagePixelStatus(int w, int h){

    int color = img.pixelColor(w,h).red();

    if ( color == 255 )
        return 1;
    else
        return 0;

}






