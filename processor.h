#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <QImage>
#include <QRgb>

/*
This is the class that does the image processing.
There is one known bug in one particular photo of a grapefruit;
 once you load image, then tap Edit-> Game of Life and then unselect Game of Life, the animation
 is still going on in the dead center of the image, as if the thresholded black/white image is in the background.

 This is yet to be resolved.

*/


class processor
{
public:
    processor();

    int thresholdValue = 128;

    void setThresholdValue(int value);
    int getThreshholdValue();

    QImage img;

    void setImg(QImage &image);
    QImage setImgThreshold();
    QImage calcGoL();


private:

    int calcPixelColorByThreshold(QColor &color);

    int getImagePixelStatus(int w, int h);

};

#endif // PROCESSOR_H
